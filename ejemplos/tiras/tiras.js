# El método JSON.stringify() convierte un valor dado en javascript a una cadena  JSON

const fs = require('fs');
fs.readFile('jsonIO/data.json', 'utf8', (err, data) => {
    if (err) {
        console.log(err);
    }
    else {
        let entrada = JSON.parse(data);
        let salida = { "z" : 0 };
        salida.z = entrada.x.length;
        console.log(JSON.stringify(salida));
        fs.writeFile('jsonIO/result.json', JSON.stringify(salida), (err) => {
            if (err) {
                console.log(err);
            }
        });    
    }
  });
