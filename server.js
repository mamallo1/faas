const express = require('express');
const app = express();
const server = require('http').Server(app);
const io = require('socket.io')(server);
const path = require('path');
const fileUpload = require('express-fileupload')
const exec = require('child_process').exec;
const crypto = require('crypto');
const fs = require('fs');
const tmp = require('tmp');
const morgan = require('morgan');

app.use(fileUpload());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
tmp.setGracefulCleanup();
app.use(morgan('combined'));


app.listen(3000, () => {
  console.log('Listening on port 3000 in ' + __dirname);
});

io.on('connection', (serverSocket) => {
  serverSocket.on('stop', () => {
    console.log('stop request received.');
    process.exit(0);
  })
})

app.get('/Register', (req, res) => {
  res.sendFile(path.join(__dirname + '/Register.html'));
});

app.post('/RegisterFunction', function(req, res) {
  if (Object.keys(req.files).length == 0) {
    console.log('No files were uploaded.');
    res.send('No files were uploaded.');
  }
  let tgzFile = req.files.tgzFile;
  // Obtiene el identificador único para el contenedor
  let fid = crypto.createHash('sha256').update(tgzFile.name).digest('hex');
  // Crea un directorio temporal debajo del directorio /tmp
  tmp.dir({ unsafeCleanup: true }, (err, tmpPath, fd, cleanupCallback) => {
    if (err) {
      console.log(err);
      res.send(err);
    }
    else {
      console.log('tmpPath: ' + tmpPath);
      // Mueve el fichero seleccionado tiras.tgz al directorio temporal
      // El fichero tiras.tgz contiene package.json y tiras.js
      tgzFile.mv(tmpPath + '/' + tgzFile.name, function(err) {
        if (err) {
          console.log(err);
          res.send(err);
        }
        else {
          // register.sh construye la imagen del contenedor de la siguiente forma
          // dockerbuild /tmp/xx tiras.tgz /home/mmata/faas 9f82a20c281d3
          exec(__dirname + '/register.sh ' + tmpPath + ' ' + tgzFile.name + ' ' + __dirname + ' ' + fid, (err, stdout, stderr) => {
            console.log(`stdout: ${stdout}`);
            console.log(`stderr: ${stderr}`);
            if (err) {
              console.log(err);
              res.send(err);
            }
            else {
              res.send(fid);
            }
          });
        }
      });
    }
  });
});

app.get('/Invoke', (req, res) => {
  res.sendFile(path.join(__dirname + '/Invoke.html'));
});

app.post('/InvokeFunction', (req, res) => {
  invoke(res, req.body.fid, req.body.params)
});

app.post('/InvokeFunction/:fid', (req, res) => {
  invoke(res, req.params.fid, req.body.params)
});

function invoke(res, fid, params) {
  console.log(fid + '(' + params + ')');
  // Crea un nuevo directorio temporal para data.json y result.json  
  tmp.dir({ unsafeCleanup: true }, (err, tmpPath, fd, cleanupCallback) => {
    if (err) {
      console.log(err);
      res.send(err);
    }
    else {
      console.log('tmpPath: ' + tmpPath);
      let paramFile = tmpPath + '/data.json';
      let resultFile = tmpPath + '/result.json';
      // params contiene una entrada del tipo {"x","cadena de texto"}
      fs.writeFile(paramFile, params, (err) => {
        if (err) {
          console.log(err);
          res.send(err);
        }
        else {
          fs.writeFile(resultFile, '', (err) => {
            if (err) {
              console.log(err);
              res.send(err);
            }
            else {
              exec(__dirname + '/invoke.sh ' + fid + ' ' + tmpPath, (err, stdout, stderr) => {
                console.log(`stdout: ${stdout}`);
                console.log(`stderr: ${stderr}`);
                if (err) {
                  console.log(err);
                  res.send(err);
                }
                else {
                  fs.readFile(resultFile, 'utf8', (err, result) => {
                    if (err) {
                      console.log(err);
                      res.send(err);
                    }
                    else {
                      res.send(result);
                    }
                  });
                }
              });
            }
          });    
        }
      });
    }
  });
}
