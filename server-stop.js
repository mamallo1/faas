const io = require('socket.io-client');
const clientSocket = io.connect('http://localhost:3000');

clientSocket.on('connect', () => {
    clientSocket.emit('stop');
    console.log('stop request sent.');
    setTimeout(() => {
        process.exit(0);    
    }, 1000);
})
