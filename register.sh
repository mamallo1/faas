TMPDIR=$1
TGZFILE=$2
DOCKERDIR=$3
FID=$4

echo
echo TMPDIR: $TMPDIR
echo TGZFILE: $TGZFILE
echo DOCKERDIR: $DOCKERDIR
echo FID: $FID

cd $TMPDIR
tar -xzf $TGZFILE

cp $DOCKERDIR/Dockerfile .
mkdir jsonIO

docker build -t mmata/$FID:latest .
