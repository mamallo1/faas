FID=$1
DATADIR=$2

#FID: Identificador de la imagen del contenedor
#DATADIR: Directorio temporal donde se almacenan los ficheros data.json y result.json

echo FID: $FID
echo DATADIR: $DATADIR

docker run -v $DATADIR:/faas/jsonIO mmata/$FID
