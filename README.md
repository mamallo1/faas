# Project Title

Faas Development: Counting the number of characters in a string

## Getting Started

Copy the whole /faas directory and run the project on your own desktop

### Prerequisites

OS Linux

NodeJs

Docker



### Installing

Node
$ sudo yum install epel-release

$ sudo yum install nodejs


Docker
$ sudo yum check-update

$ curl -fsSL https://get.docker.com/ | sh

$ sudo systemctl start docker

$ sudo systemctl status docker

## Running the tests

$ node server.js
http://localhost:3000/Register  
/* Select tiras.tgz , returns id */

http://localhost:3000/Invoke  
/* Register id previous and any parameter as {"x":"character string"} */
/* Returns the length of the "character string in this format: {"z":16} */


## Deployment

Copy the whole /faas directory and run the project on your own desktop
To start project
$ node server.js

## Authors

Manuel Mata
