FROM node:alpine
WORKDIR /faas
COPY package.json .
RUN npm install
COPY . .
CMD ["npm", "start"]